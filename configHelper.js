let configHelper = new function () {
    this.version = '0.1.3-SNAPSHOT';
    this.defaultNamespace = '<namespace>';
    this.getNamespace = (namespace) => {
        return ( namespace ? namespace : this.defaultNamespace);
    }
    this.getConfigKey = (namespace) => {
        return this.getNamespace(namespace) + '.config';
    }
    this.configToPrettyString = function(key, pretty) {
        return ( pretty
                ? key + '\n' + JSON.stringify(JSON.parse(GM_getValue(key)), null, 2)
                : '\t' + key + '\n\t' + GM_getValue(key) );
    }
    this.updateConfig = () => {
        let key = prompt('Enter configuration key', this.getConfigKey());
        if ( key != null && key != '' ) {
            let configStr = prompt('Enter configuration JSON', GM_getValue(key, '{"propertie": "value"}'));
            if ( configStr != null && configStr != '' ) {
                GM_setValue(key, configStr);
                alert('Configuration saved as:\n' + this.configToPrettyString(key));
            } else {
                alert('Value should not be empty.');
            }
        } else {
            alert('Key should not be empty.');
        }
    }
    this.showConfig = () => {
        let key = prompt('Enter configuration key', this.getConfigKey());
        if ( key != null && key != '' ) {
            alert('Configuration:\n' + this.configToPrettyString(key, true));
        } else {
            alert('Key should not be empty.');
        }
    }
    this.deleteConfig = () => {
        let key = prompt('Enter configuration key', this.getConfigKey());
        if ( key != null && key != '' ) {
            if ( confirm('Are you sure you would like to delete config:\n' +
                         this.configToPrettyString(key)) ) {
                GM_deleteValue(key);
                alert('Configuration deleted successfully.');
            }
        } else {
            alert('Key should not be empty.');
        }
    }
    this.getConfigObject = (configString, parseErrorAlertMessage, postProcessing = (obj) => {} ) => {
        let obj = null;
        try {
            obj = JSON.parse(configString);
            postProcessing(obj);
        } catch (e) {
            if ( parseErrorAlertMessage != null && parseErrorAlertMessage != '' ) {
                alert(parseErrorAlertMessage);
            }
            GM_log('Can\'t parse and build config string. Exception:')
            GM_log(e.message)
        }
        return obj;
    }
    this.getConfigString = (namespace) => {
        return GM_getValue(this.getConfigKey(namespace));
    }
    this.getConfig = (namespace) => {
        return this.getConfigObject(this.getConfigString(namespace));
    }
    this.addConfigMenu = (namespace, defaultConfig) => {
        this.defaultNamespace = namespace;
        let configKey = this.getConfigKey(namespace);
        if ( defaultConfig != null && GM_getValue(configKey) == null ) {
            GM_setValue(configKey, defaultConfig);
        }
        GM_registerMenuCommand('Update Config', configHelper.updateConfig, 'u');
        GM_registerMenuCommand('Show Config', configHelper.showConfig, 's');
        GM_registerMenuCommand('Delete Config', configHelper.deleteConfig, 'd');
    }
};
