## Intro ##

This repository is used to store and version common pieces of JavaScript code
that could be reused by Tampermonkey Scripts Creators 

### Content ###

* `configHelper` - unified handling of configs stored in Tampermonkey storage
per script

## Contribution guidelines ##

If you would like to contribute - create a pull request.

If you need some features or would like to propose some features - create
an issue.


## Release Notes ##

## v. 0.1.3 ##

### Features ###

* Added possibility to get namespace (`getNamespace`) and config key (`getConfigKey`).

### Enhancements ###

* Code refactoring and simplification


## v. 0.1.2 ##

### Features ###

* Added possibility to define and store default config if was not stored earlier.
